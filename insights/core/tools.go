// SPDX-FileCopyrightText: 2020,  Lightmeter <hello@lightmeter.io>
// SPDX-License-Identifier: AGPL-3.0

// +build tools

package core

import _ "github.com/golang/mock/mockgen"
